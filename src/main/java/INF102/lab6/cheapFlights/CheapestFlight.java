package INF102.lab6.cheapFlights;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {

        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();
        for (Flight flight : flights) {
            graph.addVertex(flight.start);
            graph.addVertex(flight.destination);
            graph.addEdge(flight.start, flight.destination, flight.cost);
        }
        return graph;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        if (nMaxStops == 0) {
            return findCheapestFlightsNoStops(flights, start, destination);
        } else {
            return findCheapestFlightsWithStops(flights, start, destination, nMaxStops);
        }
    }

    private int findCheapestFlightsNoStops(List<Flight> flights, City start, City destination) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        return graph.getWeight(start, destination);
    }

    private int findCheapestFlightsWithStops(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        int cheapestCost = dfs(start, 0, 0, destination, nMaxStops, graph);
    
        if (cheapestCost >= Integer.MAX_VALUE) {
            return -1;
        } else {
            return cheapestCost;
        }
    }
    
    private int dfs(City currentCity, int currentCost, int stops, City destination, int nMaxStops, WeightedDirectedGraph<City, Integer> graph) {
        if (currentCity == destination) {
            return Math.min(currentCost, Integer.MAX_VALUE);
        }
        if (stops > nMaxStops || currentCost >= Integer.MAX_VALUE) {
            return Integer.MAX_VALUE; // Prune the search if too many stops or high cost
        }
    
        int minCost = Integer.MAX_VALUE; // Initialize with the current cheapest cost
    
        for (City nextCity : graph.outNeighbours(currentCity)) {
            int nextCost = currentCost + graph.getWeight(currentCity, nextCity);
            int routeCost = dfs(nextCity, nextCost, stops + 1, destination, nMaxStops, graph);
            if (routeCost < minCost) {
                minCost = routeCost;
            }
        }
    
        return minCost;
    }

    //use bellman ford algorithm
    

    // private int findCheapestFlightsWithStops(List<Flight> flights, City start, City destination, int nMaxStops) {
    //     WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
    //     int cheapestCost = dfs(start, 0, 0, destination, nMaxStops, Integer.MAX_VALUE, graph);
    //     return (cheapestCost == Integer.MAX_VALUE) ? -1 : cheapestCost;
    // }
    
    // private int dfs(City currentCity, int currentCost, int stops, City destination, int nMaxStops, int cheapestCost, WeightedDirectedGraph<City, Integer> graph) {
    //     if (currentCity == destination) {
    //         return Math.min(cheapestCost, currentCost);
    //     }
    //     if (stops > nMaxStops || currentCost >= cheapestCost) {
    //         return cheapestCost; 
    //     }
        
    //     int minCost = cheapestCost; // Initialize with the current cheapest cost
    
    //     for (City nextCity : graph.outNeighbours(currentCity)) {
    //         int nextCost = currentCost + graph.getWeight(currentCity, nextCity);
    //         minCost = Math.min(minCost, dfs(nextCity, nextCost, stops + 1, destination, nMaxStops, minCost, graph));
    //     }
    
    //     return minCost;
    // }
    

}
